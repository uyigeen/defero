package be.kdg.defero.proxy;

import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.TextComponent;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * BUNGEECORD
 */
public class DeferoServer
{
    /**
     * By default, debug mode is turned on which
     * means you will get to see traffic in the
     * BungeeCord instance.
     */
    public static boolean DEBUG = true;

    /**
     * Defining local variables of the server.
     */
    private ServerSocket serverSocket;
    private Thread thread;

    /**
     * Contrsuctor
     * @param port The port where Defero will be hosted on.
     */
    public DeferoServer(int port)
    {
        try
        {
            /*
             * Create a new socket instance where we listen to clients.
             */
            this.serverSocket = new ServerSocket(port);

            thread = new Thread(() ->
            {
                try
                {
                    while (!serverSocket.isClosed())
                    {
                        /*
                         * Once accept() gets ran, it means a new
                         * client has been connected to the server.
                         */
                        Socket socket = serverSocket.accept();
                        ClientHandler clientHandler = new ClientHandler(socket);

                        new Thread(clientHandler).start();
                    }
                }
                catch (IOException exception)
                {
                    thread.interrupt();
                }
            });

            thread.start();
        }
        catch (IOException exception)
        {
            exception.printStackTrace();
        }
    }

    /*
     * This method will be used in the onDisable() in your
     * Spigot server when you close the server.
     *
     * It tells the server you opt out and your client
     * will get removed from the client handlers.
     */
    public void stop()
    {
        try
        {
            if (serverSocket != null)
                serverSocket.close();

            BungeeCord.getInstance().getConsole().sendMessage(
                new TextComponent(ChatColor.AQUA + "Thanks for using Defero, bye.")
            );
        }
        catch (IOException exception)
        {
            exception.printStackTrace();
        }
    }
}
