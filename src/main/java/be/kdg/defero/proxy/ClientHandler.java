package be.kdg.defero.proxy;

import net.md_5.bungee.BungeeCord;
import static net.md_5.bungee.api.ChatColor.*;
import net.md_5.bungee.api.chat.TextComponent;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

class ClientHandler implements Runnable
{
    /*
     * This is a static array which holds all info about
     * the connected clients to the proxy server.
     */
    public static List<ClientHandler> clientHandlers = new ArrayList<>();

    /**
     * Socket for a connection, buffer reader and writer
     * for receiving and sending data respectively.
     */
    private Socket socket;
    private BufferedReader bufferedReader;
    private BufferedWriter bufferedWriter;
    private String serverName;

    /**
     * Constructor
     * @param socket The socket which was given from accept().
     */
    public ClientHandler(Socket socket)
    {
        try
        {
            this.socket = socket;

            this.bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));
            this.serverName = bufferedReader.readLine();

            clientHandlers.add(this);

            if (DeferoServer.DEBUG)
                log(AQUA + "Server: '" + serverName + "' has been connected to Defero.");
        }
        catch (IOException exception)
        {
            close(socket, bufferedReader, bufferedWriter);
        }
    }

    @Override
    public void run()
    {
        String blob;

        /*
         * Continue to listen for messages while a connection
         * with the client is still established.
         */
        while (socket.isConnected())
        {
            try
            {
                blob = bufferedReader.readLine();
                String[] recipients = blob.split("::")[1].split(",");

                if (DeferoServer.DEBUG)
                {
                    String[] array = blob.split("::");
                    String data = array[2];

                    log(STRIKETHROUGH + "------------------------------------------");
                    log(GRAY + "Sender: " + GREEN + array[0]);
                    log(GRAY + "Receiver(s): " + GREEN + Arrays.toString(recipients));
                    log("-");
                    log(GRAY + "Raw: '" + AQUA  + blob + GRAY + "'");
                    log(GRAY + "Data: '" + AQUA + data + GRAY + "'");
                    log(STRIKETHROUGH + "------------------------------------------");
                }

                broadcast(blob, recipients);
            }
            catch (IOException exception)
            {
                close(socket, bufferedReader, bufferedWriter);
                break;
            }
        }
    }

    /**
     * @param servers The receivers of the data message.
     * @param data String data which gets broadcasted to
     *             all clients in the array list.
     */
    public void broadcast(String data, String... servers)
    {
        for (ClientHandler clientHandler : clientHandlers)
        {
            try
            {
                /*
                 * You don't want to send the data to the
                 * server who sent it in the first place.
                 */
                if (clientHandler.serverName.equals(serverName))
                    continue;

                /*
                 * This data is only specified for 1 or more servers
                 */
                if (servers.length > 1 && !servers[0].equals("*"))
                {
                    for (String serverName : servers)
                    {
                        if (clientHandler.serverName.equals(serverName))
                        {
                            clientHandler.bufferedWriter.write(data);
                            clientHandler.bufferedWriter.newLine();
                            clientHandler.bufferedWriter.flush();
                            break;
                        }
                    }
                }
                else
                {
                    clientHandler.bufferedWriter.write(data);
                    clientHandler.bufferedWriter.newLine();
                    clientHandler.bufferedWriter.flush();
                }
            }
            catch (IOException exception)
            {
                close(socket, bufferedReader, bufferedWriter);
            }
        }
    }

    /**
     *
     * @param socket The client's connected socket
     * @param bufferedReader The client's buffered reading stream.
     * @param bufferedWriter The client's buffered writing stream.
     */
    public void close(Socket socket, BufferedReader bufferedReader, BufferedWriter bufferedWriter)
    {
        removeClient();

        try
        {
            if (bufferedReader != null)
                bufferedReader.close();

            if (bufferedWriter != null)
                bufferedWriter.close();

            /*
             * By closing the socket, it also automatically
             * closes all input and output streams with it.
             */
            if (socket != null)
                socket.close();
        }
        catch (IOException exception)
        {
            exception.printStackTrace();
        }
    }

    public void removeClient()
    {
        clientHandlers.remove(this);

        if (DeferoServer.DEBUG)
            log(DARK_AQUA + "Server: '" + serverName + "' has been disconnected from Defero.");
    }

    private void log(String message)
    {
        BungeeCord.getInstance().getConsole().sendMessage(
                new TextComponent(message)
        );
    }
}
