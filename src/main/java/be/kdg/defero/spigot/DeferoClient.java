package be.kdg.defero.spigot;

import be.kdg.defero.proxy.DeferoServer;
import be.kdg.defero.spigot.event.IncomingEvent;
import net.md_5.bungee.BungeeCord;
import net.md_5.bungee.api.chat.TextComponent;
import org.bukkit.Bukkit;

import java.io.*;
import java.net.Socket;
import java.util.Arrays;

import static net.md_5.bungee.api.ChatColor.*;

/**
 * SPIGOT
 */
public class DeferoClient
{
    /**
     * By default, debug mode is turned on which
     * means you will get to see incoming data
     * in the Spigot console.
     */
    public static boolean DEBUG = true;

    /**
     * Defining local variables of the client.
     */
    private Socket socket;
    private BufferedReader bufferedReader;
    private BufferedWriter bufferedWriter;
    private String serverName;

    /**
     * Constructor
     * @param port The port where Defero will be listened on.
     * @param serverName This will be used to identify the server.
     */
    public DeferoClient(int port, String serverName)
    {
        try
        {
            /*
             * Create a new socket instance which will connect
             * to the proxy server.
             */
            this.socket = new Socket("localhost", port);
            this.serverName = serverName;

            /*
             * Create a buffered reader and writer from the socket's I/O streams.
             */
            this.bufferedReader = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            this.bufferedWriter = new BufferedWriter(new OutputStreamWriter(socket.getOutputStream()));

            /*
             * For as long the socket holds a connection with
             * the server, this method will get executed to
             * listen for incoming data.
             */
            listen();

            /*
             * Identify this server in the proxy server.
             */
            identify();
        }
        catch (IOException exception)
        {
            exception.printStackTrace();
        }
    }

    /**
     * This method gets executed whenever a client attempts
     * to connect with the proxy.
     *
     * The client's server name gets sent to the proxy where
     * the proxy then knows this server exists and can sent
     * data over to this server.
     */
    private void identify()
    {
        try
        {
            if (socket.isConnected())
            {
                /*
                 * Sending the data over and manually flushing it.
                 */
                takeOff(serverName);
            }
        }
        catch (IOException exception)
        {
            close(socket, bufferedReader, bufferedWriter);
        }
    }

    /**
     * @param data Data that gets sent. You can be creative with
     *             this. This can be JSON-formatted data which
     *             can be used to deserialize to an object and
     *             use it like that.
     *
     * @param servers A list of servers which this data has to
     *                get sent to. If this paramater is null,
     *                then it will get sent to all those who
     *                are connected.
     */
    public void sendData(String data, String... servers)
    {
        try
        {
            if (socket.isConnected())
            {
                /*
                 * If servers has no arguments, it will assume that
                 * you want to send it to all servers.
                 */
                String[] receivers = servers != null ? servers : new String[]{"*"};

                /*
                 * Encoded string with all data necessary to find its
                 * receiver(s) and to get the data to.
                 */
                String blob = serverName + "::" + String.join(",", receivers) + "::" + data;

                /*
                 * Sending the data over and manually flushing it.
                 */
                takeOff(blob);
            }
        }
        catch (IOException exception)
        {
            close(socket, bufferedReader, bufferedWriter);
        }
    }

    /*
     * For as long the socket holds a connection with
     * the server, this method will get executed to
     * listen for incoming data.
     */
    private void listen()
    {
        new Thread(() ->
        {
            while (socket.isConnected())
            {
                String blob;

                try
                {
                    /*
                     * Whenever readLine() gets executed, it means
                     * new incoming data has been received.
                     */
                    blob = bufferedReader.readLine();

                    String[] array = blob.split("::");
                    String data = array[2];

                    if (DEBUG)
                    {
                        log(STRIKETHROUGH + "------------------------------------------");
                        log(GRAY + "Sender: " + GREEN + array[0]);
                        log("-");
                        log(GRAY + "Raw: '" + AQUA  + blob + GRAY + "'");
                        log(GRAY + "Data: '" + AQUA + data + GRAY + "'");
                        log(STRIKETHROUGH + "------------------------------------------");
                    }

                    /*
                     * Making it easily accessible to the entire
                     * server by calling a Bukkit event to it.
                     */
                    Bukkit.getServer().getPluginManager().callEvent(new IncomingEvent(data));
                }
                catch (IOException exception)
                {
                    close(socket, bufferedReader, bufferedWriter);
                }
            }

        }).start();
    }

    /**
     * @param socket The client's connected socket
     * @param bufferedReader The client's buffered reading stream.
     * @param bufferedWriter The client's buffered writing stream.
     */
    private void close(Socket socket, BufferedReader bufferedReader, BufferedWriter bufferedWriter)
    {
        try
        {
            if (bufferedReader != null)
                bufferedReader.close();

            if (bufferedWriter != null)
                bufferedWriter.close();

            /*
             * By closing the socket, it also automatically
             * closes all input and output streams with it.
             */
            if (socket != null)
                socket.close();
        }
        catch (IOException exception)
        {
            exception.printStackTrace();
        }
    }

    /**
     * @param data The data that gets sent.
     * @throws IOException Exception
     */
    private void takeOff(String data) throws IOException
    {
        /*
         * Sending the data over and manually flushing it.
         */
        bufferedWriter.write(data);
        bufferedWriter.newLine();
        bufferedWriter.flush();
    }

    private void log(String message)
    {
        Bukkit.getConsoleSender().sendMessage(message);
    }
}
