package be.kdg.defero.spigot.event;

import org.bukkit.event.Event;
import org.bukkit.event.HandlerList;

/**
 * Making it easily accessible to the entire
 * server by calling a Bukkit event to it.
 */
public class IncomingEvent extends Event
{
    private static final HandlerList handlers = new HandlerList();
    private final String data;

    public IncomingEvent(String data)
    {
        this.data = data;
    }

    public String getData()
    {
        return data;
    }

    public HandlerList getHandlers()
    {
        return handlers;
    }

    public static HandlerList getHandlerList()
    {
        return handlers;
    }
}
