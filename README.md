# ![Logo](https://i.imgur.com/twSPlU5.png)

# Defero is a lightweight API which allows servers to communicate with each other using sockets in a modern way.

![Bungee](https://i.imgur.com/aZFMtyO.png)
**This is the only code you need to write to initialize Defero on your BungeeCord
 and create a** **Defero server that listens on any port you choose.**

```java
public final class Main extends Plugin 
{
	public DeferoServer defero;
	
	@Override
	public void onEnable()
	{
		//Debug mode is on
		DeferoServer.DEBUG = true;
		server = new DeferoServer(1132);
	}

	@Override
	public void onDisable() 
	{
		defero.stop();
	}
}
```

![Spigot](https://i.imgur.com/Wc2VItB.png)

```java
public final class DeferoSpigot extends JavaPlugin implements Listener 
{
	public DeferoClient client;

	@Override
	public void onEnable() 
	{
		getServer().getPluginManager().registerEvents(this, this);
		
		// Identify your server (e.g. Lobby-1, Lobby-2...)
		String serverName = getConfig().getString("server.name");
		client = new DeferoClient(1132, serverName);
		
		// This will only get sent to Lobby-2
		client.sendData("{message: \"Hello, world.\"}", "Lobby-2");
		
		// This will get sent to all connected servers
		client.sendData("{message: \"Hello, world.\"}");
	}
	
	@EventHandler
	public void onMessage(IncomingEvent event) 
	{
		// for e.g. {message: \"Hello, world.\"}
		System.out.println(event.getData());
	}
}
```

![Output](https://i.imgur.com/jaS6XKi.png)

**If you have Debug mode on, you will get to see
live traffic data in the BungeeCord console.**
![Console](https://i.imgur.com/A34SuB6.png)
